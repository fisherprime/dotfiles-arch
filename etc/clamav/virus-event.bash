#!/bin/bash

# REF: https://wiki.archlinux.org/index.php/ClamAV

PATH=/usr/bin
alert="Signature detected: $CLAM_VIRUSEVENT_VIRUSNAME in $CLAM_VIRUSEVENT_FILENAME"

if [[ -z $(command -v systemd-cat) ]]; then
	# Send alert to "var/log"
	echo "$(date) - $alert" >>/var/log/clamav/detections.log
else
	# Send alert to the system logger.

	# NOTE: The `emerg` priority pollutes "DISPLAY"s with the clamav warning;
	# shows up in tmux windows.
	# echo "$alert" | /usr/bin/systemd-cat -t clamav -p emerg

	logger -t clamav -p warning "$alert"
fi

/usr/local/bin/broadcast-notification.sh -u critical -i "clamav" \
	"Malware detected" "$alert"

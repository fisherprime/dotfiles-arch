#!/bin/bash

# Execute handler operations for low battery charge scenarios.
#
# Arguments: <excution-identifier> <charge-scenario>
#
# The execution identifier is "exec-this"; signals this file's function should
# be executed.
case $1 in
suspend)
	/usr/local/bin/broadcast-notification.sh -i battery-caution-symbolic -u critical \
		'Critical battery level' 'System suspending'
	/usr/bin/sleep 5 && /usr/bin/systemctl suspend
	;;
hybrid-sleep)
	/usr/local/bin/broadcast-notification.sh -i battery-caution-symbolic -u critical \
		'Critical battery level' 'System going into hybrid sleep'
	/usr/bin/sleep 5 && /usr/bin/systemctl hybrid-sleep
	;;
inform)
	/usr/local/bin/broadcast-notification.sh -i battery-low-symbolic -u critical -t 60000 \
		'Low battery level' 'System will suspend soon'
	;;
full)
	/usr/local/bin/broadcast-notification.sh -i battery-full-symbolic -u normal -t 10000 \
		'Battery charged' 'Unplug charger'
	;;
debug)
	/usr/local/bin/broadcast-notification.sh -i battery-full-symbolic -u low -t 10000 \
		'Battery notification test' 'Test succeeded'
	;;
*) ;;
esac

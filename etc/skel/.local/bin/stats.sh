#!/bin/bash

read_array_cmd() {
	if [[ -n $BASH_VERSION ]]; then
		\read -ra "$@"
	else
		# Zsh & other shells.
		\read -Ar "$@"
	fi
}

# ----
nvidia_temp() {
	local gpu_state=0

	# The escaped colours won't work with polybar

	if [[ -e /proc/acpi/bbswitch ]]; then
		# Bumblebee is installed
		[[ "$(cut -f 2 -d " " /proc/acpi/bbswitch)" == "ON" ]] && gpu_state=1

		# FIXME: Needs to validate the GPU is usable before querying it.
	else
		# No bumblebee
		[[ $(($(pgrep -c nvidia) + 0)) -gt 1 ]] && gpu_state=1
	fi

	if [[ $gpu_state -eq 1 ]]; then
		IFS="," read_array_cmd metrics <<<"$(nvidia-smi --query-gpu=name,index,temperature.gpu,power.draw --format=csv,noheader,nounits)"

		# shellcheck disable=SC2154 # Set in the line above.
		[[ ${metrics[0]} != "NVIDIA "* ]] &&
			printf 'GPU unavailable (failure)\n' &&
			return 1

		local name="${metrics[0]}"
		local index="${metrics[1]/ /}"
		local temperature="${metrics[2]}"
		local power_draw="${metrics[3]}"

		case $temperature in
		[0-1][0-9])
			# icon="\e[1;32m  \e[0m"
			icon=
			;;
		[2-3]*)
			# icon="\e[1;32m  \e[0m"
			icon=
			;;
		[4-5]*)
			# icon="\e[1;32m  \e[0m"
			icon=
			;;
		[6-7]*)
			# icon="\e[1;33m  \e[0m"
			icon=
			;;
		*) # 100+ & everything else
			# icon="\e[1;31m  \e[0m"
			icon=
			;;
		esac

		printf "%s (%s): $icon %.2f°C,  %.2fW\n" "$name" "$index" "$temperature" "$power_draw"
		# \u00b0 == ° \uf469

		return
	fi

	# Don't echo anything dGPU is off

	# echo -e "\e[1;32m GPU \e[0m"
	printf ''
} # End nvidia_temp

bluetooth_status() {
	if [[ "$(rfkill -o SOFT list bluetooth)" == *"unblocked"* ]]; then
		power_state="$(bluetoothctl show | grep Powered | cut -d : -f 2)"

		[[ $power_state == "" ]] && printf '\n' && return

		if [[ $power_state =~ \s?yes ]]; then
			printf '\n'
			# \uf294 (NF)

			return
		fi
		printf '\n'
		# \uf5b1 (NF)

		return
	fi

	printf '\n'
} # bluetooth_status
# ----

if [[ $# -eq 1 ]]; then
	$1
else
	$1 "$@"
fi

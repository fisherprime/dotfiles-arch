#!/bin/bash

# Broadcast a notification to all logged in users with a display session.
#
# Arguments: <excution-identifier> <urgency> <summary> <body(optional)> <icon(optional)>
broadcast() {
	exec_path="/usr/bin"
	notify_prog="$exec_path/notify-send"

	# Obtain user list.
	# shellcheck disable=SC2207,SC2016
	users=($("$exec_path/who" | "$exec_path/awk" '{print $1}' | "$exec_path/sort" -u))

	for user in "${users[@]}"; do
		dbus_address="unix:path=/run/user/$("$exec_path/id" -u "$user")/bus"

		# Debug
		# printf "User: %s, dbus address:\n" "$user" "$dbus_address"

		"$exec_path/sudo" -u "$user" PATH="$PATH" DBUS_SESSION_BUS_ADDRESS="$dbus_address" \
			"$notify_prog" "$@"
	done
}

# Debug
# printf "Num args: %s, args: %s\n" "$#" "$*"

# NOTE: Array slicing doesn't work; ${@[@]:1}
[[ $# -gt 1 ]] &&
	broadcast "$@" && exit

printf "$(notify-send --help | sed -E s/notify-send/broadcast-notification.sh/g)\n"

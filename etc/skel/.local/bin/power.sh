#!/bin/bash

# ----
timeout=1

# power_status() {
# BAT=/sys/class/power_supply/BAT0
# echo $(($(cat $BAT/current_now) * $(cat $BAT/voltage_now) / 1000000000000))
# } # End power_status

sys_shutdown() {
	if [[ $2 == "now" ]]; then
		notify-send -i "system-shutdown" -u critical "Poweroff" "Shutting down"
		shutdown now

		return
	fi

	[[ $2 =~ \d+ ]] && timeout=$2
	echo "$timeout"

	shutdown +"$timeout"
	notify-send -i "system-shutdown" -u critical "Shutting down in $timeout min"
} # End sys_shutdown

sys_reboot() {
	if [[ $2 == "now" ]]; then
		notify-send -i "system-restart" -u critical "Reboot" "Rebooting"
		shutdown -r now

		return
	fi

	[[ $2 =~ \d+ ]] && timeout=$2
	echo "$timeout"

	shutdown -r +"$timeout"
	notify-send -i "system-restart" -u critical "Reboot" "Rebooting in $timeout (min)"
} # End sys_reboot
# ----

if [[ $# -eq 1 ]]; then
	$1
else
	$1 "$@"
fi

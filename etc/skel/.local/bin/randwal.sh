#!/bin/bash

# Generate colourscheme from a random wallpaper & update steam colour scheme.
# Recurse through wallpaper directory directory provided by the export:
# WALLPAPERS="$HOME/Wallpaper"

# NOTE: This script cannot select an image best fit for a monitor's
# resolution.

_cleanup() {
	pkill -9 wal
}

get_wallpaper_dirs() {
	wallpapers_array=($(tr : '\n' <<<$WALLPAPERS))

	for wallpapers_item in "${wallpapers_array[@]}"; do
		while IFS=$'\n' read -r val; do
			num_images=$(
				find -L "$val" -maxdepth 1 -type f -exec file {} \; |
					grep -ic image
			)

			[[ $num_images -gt 0 ]] && wallpaper_dirs+=("$val")
		done <<<"$(find -L "$wallpapers_item" -maxdepth 5 -type d)"
	done
}

fallback_default_backend() {
	printf "[I] Fallback to default backend: %s\n" "$default_backend"

	# Debug
	# shellcheck disable=SC2086
	# echo wal --backend "$default_backend" ${additional_flags[*]}

	# shellcheck disable=SC2086
	output=$(
		wal --backend "$default_backend" ${additional_flags[*]}
	)

	[[ "$output" != *"Exported all"* ]] &&
		printf "\n[E] Could not generate a colour scheme for options: \"%s\"" \
			"${additional_flags[*]}"
}

set_wallpaper() {
	wallpaper_dirs_length=${#wallpaper_dirs[*]}
	if [[ $wallpaper_dirs_length -lt 1 ]]; then
		printf "[E] No wallpaper directory\n"
		exit 1
	fi

	dir_index="$((RANDOM % wallpaper_dirs_length))"
	additional_flags+=("-i" "${wallpaper_dirs[dir_index]}")

	output=$(
		# shellcheck disable=SC2086
		wal --backend "$preferred_backend" ${additional_flags[*]}
	)

	# Debug
	printf "Output: %s\n" "$output"

	[[ "$output" != *"Exported all"* ]] &&
		fallback_default_backend
}

main() {
	# Run: `wal --backend` for the available choices.
	# NOTE: Haishoku produces best results for my case; but, encounters "Index out
	# of range" errors for some monochrome images.
	# The `python-haishoku` AUR package provides the haishoku backend.

	# Any unknown backend choice is resolved to "wal" by pywal, else, this
	# script defaults to wal for a misspelled backend.
	default_backend="wal"
	preferred_backend="haishoku"

	wallpaper_dirs=()
	additional_flags=("--iterative")

	[[ $# -gt 0 ]] &&
		additional_flags+=("$@")

	trap "_cleanup" SIGINT SIGQUIT SIGABRT

	# Debug.
	# printf "WALLPAPERS: %s\n" "$WALLPAPERS"
	get_wallpaper_dirs

	set_wallpaper

	exit 0
}

# [[ -d ${WALLPAPERS:-$HOME/Wallpaper} ]] &&
main "$@"

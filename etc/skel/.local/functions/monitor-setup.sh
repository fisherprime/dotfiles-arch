#
# ~/.local/functions/monitors.sh
#

export_monitors() {
	export_x_monitors
	# export_wayland_monitors
}

export_x_monitors() {
	[[ ! -x $(command -v xrandr) ]] && return 1

	counter=0

	IFS=$'\n' read_array_cmd monitors <<<"$(xrandr --listmonitors | tail -n 1 |
		cut -f 6 -d ' ')"

	# Debug.
	# printf "Monitors: %s\n" "${monitors[*]}"

	for monitor in "${monitors[@]:?}"; do
		eval "export MONITOR$counter=\"$monitor\""
		counter=$((counter + 1))
	done
}

# export_wayland_monitors() {
#
# }

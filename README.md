---

<style>
:root {
  --black-col-1: #202020;
  --black-col: #111;
  --blue-col-1: #4ca3ff;
  --blue-col: #2a7ae2;
  --cyan-col: #277086;
  --green-col-1: #0c9;
  --green-col: #009926;
  --grey-col-dark: #424242;
  --grey-col-darker: #282828;
  --grey-col-light: #c2c2c2;
  --grey-col: #828282;
  --red-col-1: #d14;
  --red-col: #a61717;
  --white-col: #fdfdfd;
  --why-mess-markdown: PERMIT-STYLE-TAGS-GITLAB;
  --yellow-green-col: #b5e853;
}

body,
h1,
h2,
li,
p,
span,
summary {
  background-color: var(--grey-col-darker) !important;
  color: var(--grey-col-light) !important;
}

a {
  color: var(--yellow-green-col) !important;
}

code {
  color: var(--grey-col-darker);
}
</style>

---

# Dotfiles

A collection of assorted dotfiles from my Arch Linux install.

## System details

    - OS: **Arch Linux**.
    - Window Manager: **sway/i3**.
    - Desktop Environment: **None**.
    - Login Manager/Display Manager: **None** (prefer logging in from a tty).

## Miscellaneous notes

### Application packages

Necessary pacman packages in
[packages/pacman.txt](https://gitlab.com/fisherprime/dotfiles-arch/blob/master/packages/pacman.txt).S
Install instruction:

```sh
pacman -S --needed - < packages/pacman.txt
```

If you wish to include some AUR packages, pass
[packages/packages.txt](https://gitlab.com/fisherprime/dotfiles-arch/blob/master/packages/packages.txt)
to yay, or appropriate AUR wrapper.
Install instruction:

```sh
yay -S --needed - < packages/packages.txt
```

Necessary Python3 packages in
[packages/pip3.txt](https://gitlab.com/fisherprime/dotfiles-arch/blob/master/packages/pip3.txt).
Install instruction:

```sh
pip3 install -r packages/pip3.txt
```

Miscellaneous Pipx packages in
[packages/pipx.txt](https://gitlab.com/fisherprime/dotfiles-arch/blob/master/packages/pipx.txt).

`buku` requires `pipx inject buku flask flask-admin flask-api flask-bootstrap flask-paginate
flask-wtf arrow`

`gns3-gui` requires `pipx inject gns3-gui pyqt5 pyqt5-sip`

Install instructions (if needed) are located in
[install.arch](https://gitlab.com/fisherprime/fotfiles-arch/blob/master/install.md).
**Don't overwrite your files like an idiot, review for correctness and
sanity.**

## Screenshots

**Some of the elements in the screenshots may be outdated.**

![clean](https://gitlab.com/fisherprime/dotfiles-arch/raw/master/screenshots/clean.png "Clean desktop")

![cava](https://gitlab.com/fisherprime/dotfiles-arch/raw/master/screenshots/with-cava.png "cava in alacritty")

![misc](https://gitlab.com/fisherprime/dotfiles-arch/raw/master/screenshots/misc.png "vim, ncmpcpp")

log() { logger -p daemon.warning "ACPI: $*"; }
uhd() { log "event unhandled: $*"; }

case "$2" in
# AC | ACAD | ADP0 | ACPI0003:00)
AC* | AD*)
	case "$4" in
	00000000)
		log 'AC unpluged'

		# tlp auto
		;;
	00000001)
		log 'AC pluged'

		# tlp auto
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
*)
	uhd "$@"
	;;
esac

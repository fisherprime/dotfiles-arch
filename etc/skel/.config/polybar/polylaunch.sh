#!/bin/bash

launch_polybar() {
	# Set script execution limit in seconds
	local counter
	local limit
	counter=0
	limit=30

	killall -q polybar

	# Wait for polybar to exit
	while (pgrep -x polybar >/dev/null); do
		# echo "counter: $counter, limit: $limit"
		if [[ $counter -eq limit ]]; then
			# Send SIGKILL to polybar
			pkill -9 polybar
			break
		fi

		counter=$((counter + 1))
		sleep 1
	done

	# `-r` option reloads polybar on config change
	polybar -r top &
	polybar -r bottom &

	# Ensure conky is drawn on the root window before polybar if it was
	# running
	if [[ $(pgrep -c conky) -gt 0 && -x /usr/bin/conky ]]; then
		pkill conky
		conky &
	fi
}

launch_polybar

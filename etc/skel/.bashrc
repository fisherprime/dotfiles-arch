#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# Sources
# ----
[[ -r $HOME/.aliases ]] && . "$HOME"/.aliases
[[ -r $HOME/.functions ]] && . "$HOME"/.functions

# Final shell configurations
# ----
[[ -r $HOME/.postinits ]] && . "$HOME"/.postinits

#
# ~/.local/functions/virtual-machine.sh
#

# Global variables
VM_DIR="$ALT_HOME/VM"

: <<'LEFT_OUT'
export QEMU_AUDIO_DRV=pa; \
export QEMU_PA_SERVER=/run/user/1000/pulse/native; \
-vnc 127.0.1.4:1 \
-usb -usbdevice tablet \
-serial none \
-parallel none \
-drive file=$VM_DIR/ISO/Win10_1703_EnglishInternational_x64.iso,index=3,media=cdrom \
echo "8086 1901" > "/sys/bus/pci/drivers/vfio-pci/new_id"; \
echo "0000:00:01.0" > "/sys/bus/pci/drivers/vfio-pci/0000:00:01.0/driver/unbind"; \
-bios $VM_DIR/coreboot.rom \
-bios $VM_DIR/P650.bin \
-option-rom $VM_DIR/1060_updGOP\(modded\).rom \
unset QEMU_AUDIO_DRV; \
unset QEMU_PA_SERVER; \

# IDE_TO_VFIO
# Create dummy image, then in device manager install necessary driver
-drive id=disk0,if=ide,cache=none,format=raw,file=$VM_DIR/win10.img \
-drive id=disk1,if=virtio,cache=none,format=raw,file=$VM_DIR/dummy.img \
LEFT_OUT

: <<'TO_IMPLEMENT'
# Looking glass setup on Windows guest pending
-device ivshmem-plain,memdev=looking-glass,size=32M
TO_IMPLEMENT

: <<'CASE_DEPENDENT'
echo 'OFF' > "/proc/acpi/bbswitch"
tee /proc/acpi/bbswitch <<< OFF
-serial none -parallel none" # don't use if using SPICE
CASE_DEPENDENT

: <<'NET_TAP'
export MAC_ADDR=$(printf "52:54:BE:EF:%02X:%02X\n" "$(( RANDOM % 256 ))" "$(( RANDOM % 256 ))")
sudo --preserve-env=DEF_HUGE,DEF_CPUFREQ,MAX_CPUFREQ,MEM_RES,MEM_RETRY sh -c "echo "[*] Starting gaming VM(10)"; \
	...
	-netdev type=tap,id=net0,ifname=tap0,script=tap_ifup,downscript=tap_ifdown,vhost=on \
	-device virtio-net-pci,netdev=net0,addr=19.0,mac=$MAC_ADDR'
unset MAC_ADDR
NET_TAP

_vm_on_cleanup() {
	# Perform cleanup operations for vm_on.

	# if [[ -e /dev/shm/looking-glass ]]; then \

	if [[ -z $DEF_CPUFREQ || -z $DEF_HUGE || $DEF_HUGE -gt 999 ]]; then
		export DEF_CPUFREQ
		DEF_CPUFREQ="$(cat "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq")"

		export DEF_HUGE=0
	fi

	# shellcheck disable=SC1004
	sudo --preserve-env=DEF_HUGE,DEF_CPUFREQ sh -c 'printf "[+] Performing cleanup on vm creation function\n"; \
		[[ -n "$(pgrep qemu)" ]] && pkill -e qemu; \
		sleep 5; \
		printf "[+] Hugepages: %s" "$DEF_HUGE"; \
		echo $DEF_CPUFREQ > /sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq; \
		echo $DEF_CPUFREQ > /sys/devices/system/cpu/cpu5/cpufreq/scaling_min_freq; \
		echo $DEF_CPUFREQ > /sys/devices/system/cpu/cpu6/cpufreq/scaling_min_freq; \
		echo $DEF_CPUFREQ > /sys/devices/system/cpu/cpu7/cpufreq/scaling_min_freq; \
		if [[ -e /sys/bus/pci/drivers/vfio-pci/0000:01:00.0 ]]; then \
			echo "0000:01:00.0" > /sys/bus/pci/drivers/vfio-pci/0000:01:00.0/driver/unbind; \
			printf "[+] GPU returned to host\n"; \
		fi; \
		echo $DEF_HUGE > /proc/sys/vm/nr_hugepages; \
		printf "[+] Hugepages deallocated\n"; \
		[[ -e /dev/shm/looking-glass ]] && \
			rm /dev/shm/looking-glass; \
		rmmod virtio_blk; \
		rmmod virtio_net; \
		rmmod virtio_pci; \
		rmmod virtio_ring; \
		rmmod virtio; \
		optirun printf "[+] GPU switched off\n"; \
		printf "[I] Done\n"'

	unset BIOS_BIN
	unset DEF_CPUFREQ
	unset DEF_HUGE
	unset DUMMY_IMG
	unset MAC_ADDR
	unset MAX_CPUFREQ
	unset MEM_RES
	unset MEM_RETRY
	unset OS_IMG
	unset OS_VARS
	unset SAMBA_SHARE
	unset VBIOS_ROM
	unset VIRTIO_DISK
	unset VM_NAME
	unset W10_ISO
	unset W7_ISO

	# Unset trap
	trap - SIGINT SIGTERM SIGQUIT SIGABRT EXIT

	return 0
} # End _vm_on_cleanup

vm_on() {
	# Launch a QEMU VM.
	# Using bumblebee for dGPU power management.
	# Useful for Windows only at the moment.

	# check_root

	local command_name
	command_name="$(basename "$0")"

	local command_flags
	# As MB.
	local free_memory="$((($(grep -i MemA /proc/meminfo | sed -E 's/.*:\s//; s/kB//') / 1024) - 20))"

	# My value 0.
	export DEF_HUGE
	DEF_HUGE="$(cat "/proc/sys/vm/nr_hugepages")"

	# My value 800000Hz.
	export DEF_CPUFREQ
	DEF_CPUFREQ="$(cat "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq")"

	# Single core max, should use all core max; my value 3800000.
	export MAX_CPUFREQ
	MAX_CPUFREQ="$(cat "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq")"

	# Memory to reserve in MB / hugetable size {in MB}.
	export MEM_RES
	MEM_RES="$((free_memory / 2))"

	# Times to retry setting HugePages.
	export MEM_RETRY=5

	export MAC_ADDR
	MAC_ADDR="$(printf "52:54:BE:EF:%02X:%02X\n" "$((RANDOM % 256))" "$((RANDOM % 256))")"

	# Change these.
	# `BIOS_BIN` replaces `OVMF_CODE`.
	export BIOS_BIN="$VM_DIR/Bios/bios.bin"
	export VBIOS_ROM="$VM_DIR/BIos/1060_updGOP.rom"

	export DUMMY_IMG="$VM_DIR/Storage/Win/dummy.img"
	export VIRTIO_DISK="$VM_DIR/WinISO/virtio-win-0.1.141-1.iso"
	export W10_ISO="$VM_DIR/WinISO/Win10_1703_EnglishInternational_x64.iso"
	export W7_ISO="$VM_DIR/WinISO/en_windows_7_ultimate_with_sp1_x64_dvd_u_677332.iso"

	export SAMBA_SHARE="$ALT_HOME/Games"

	_usage() {

		printf "[+] Usage: %s [options]

Options:
	-h,--help:               Print this help message
	-o,--os <vm>:            Stem name of VM files in VM directory
	\t                       In the form: <vm>.img, <vm>VARS.fd
	-m,--mem <mem>:          Memory to reserve in MB (Free: %d MB)
	-r,--retry <num>:        Number of retries when allocating hugepages (Default %d)
" "$command_name" "$free_memory" "$MEM_RETRY"

	} # End _usage

	if [[ $# -lt 1 ]]; then
		_usage
		log_w "$command_name exit with status: Invalid parameters"

		return 1
	fi

	local options
	# shellcheck disable=SC2140
	options=$(getopt -o ho:m:r: -l "help","os:","mem:","retry:" -n "$command_name" -- "$@")

	# shellcheck disable=SC2181
	if [[ $? -ne 0 ]]; then
		printf '[!] Failed parsing command options\n'
		_usage
		return 1
	fi
	eval set -- "$options"

	while [[ $1 ]]; do
		case $1 in
		-h | --help)
			_usage
			log_i "$command_name exit with status: Success"

			return 0
			;;
		-o | --os)
			if [[ $command_flags == *".-0"* ]]; then
				printf '[!] Multiple VMs selected\n'
				_usage
				log_w "$command_name exit with status: Invalid parameters"

				return 1
			fi

			shift
			if [[ -e "$VM_DIR/Storage/Win/$1.img" && -e "$VM_DIR/Code_Vars/$1VARS.fd" ]]; then
				export VM_NAME="$1"
				export OS_IMG="$VM_DIR/Storage/Win/$1.img"
				export OS_VARS="$VM_DIR/Code_Vars/$1VARS.fd"
			else
				printf "[!] The necessary VM files don't exist\n"
				log_w "$command_name exit with status: File not found"

				return 1
			fi

			command_flags+=".-o"
			;;
		-m | --mem)
			shift
			if [[ $1 -gt 0 && $1 -lt $free_memory ]]; then
				# Get rounded off reservable memory in MB, then divide the result by the hugepage
				# size to get number of hugepages to reserve.

				hugepage_size=$((\
					$(grep Hugepagesize /proc/meminfo | sed -E 's/\w+:\s+([0-9]+).+/\1/') / \
					1024))

				MEM_RES="$(((($1 / 1024) * 1024) / hugepage_size))"
			else
				printf "[+] Can't reserve that amount\n"
				log_w "$command_name exit with status: Unreservable memory amount"

				return 1
			fi

			command_flags+=".-m"
			;;
		-r | --retry)
			shift
			if [[ $1 -gt 0 && $1 -lt 20 ]]; then
				MEM_RETRY="$1"
			else
				printf '[+] Nah, that is excessive\n'
				log_w "$command_name exit with status: Too many retries"

				return 1
			fi

			command_flags+=".-r"
			;;
		--)
			break
			;;
		*)
			_usage
			log_w "$command_name exit with status: Invalid parameters"

			return 1
			;;
		esac
		shift
	done

	if [[ ${#command_flags[*]} -lt "$((3 * 2))" ]]; then
		_usage
		log_w "$command_name exit with status: Invalid parameters"

		return 1
	fi

	# optirun lspci -nnk -s 01:00.0, to get [SVID:SSID]
	# Link GPU to vfio
	# Create shared memory for looking-glass
	# Added SPICE with host-guest copy&paste
	# usb-audio works, pulseaudio, meh...
	# echo 6144 > /proc... creates 12288MB worth of hugepages @2MBs per hugepage
	# No need for extra hugepages
	# Chrt, change real-time scheduling & taskset, pin to 4 vCPUFREQ
	# Achieves user runtime "pinning" on core 2 & 3 (by index)
	# "intel_pstate=per_cpu_perf_limits" kernel parameter required
	# "-vga none" should be added after the video adapter is setup else VM won't display
	# touch /dev/shm/looking-glass; \
	# chown mbock:kvm /dev/shm/looking-glass; \
	# chmod 660 /dev/shm/looking-glass; \
	# [[ -e /dev/shm/looking-glass ]] && \
	#   rm /dev/shm/looking-glass; \
	# "-machine type=q35,accel=kvm" can be replaced with "-enable-kvm -M q35"

	[[ "$(grep -Ec 'intel_pstate=per_cpu_perf_limits' /proc/cmdline)" -lt 1 ]] &&
		printf "\n[I] \"intel_pstate=per_cpu_perf_limits\" kernel parameter required\n"

	#echo "Img:$OS_IMG Vars:$OS_VARS HPages:$MEM_RES" # Test

	# Cleanup on Interrupt and exit.
	trap "_vm_on_cleanup" SIGINT SIGTERM SIGQUIT SIGABRT EXIT

	{

		# shellcheck disable=SC1004
		sudo --preserve-env=BIOS_BIN,DEF_HUGE,DEF_CPUFREQ,MAX_CPUFREQ,MEM_RES,MEM_RETRY,VIRTIO_DISK,W10_ISO,W7_ISO,DUMMY_IMG,SAMBA_SHARE,VM_NAME,OS_IMG,OS_VARS,VBIOS_ROM,MAC_ADDR \
			sh -c 'printf "[*] Starting $VM_NAME VM\n"; \
		modprobe virtio; \
		modprobe virtio_ring; \
		modprobe virtio_blk; \
		modprobe virtio_net; \
		modprobe virtio_pci; \
		for counter in {1..$MEM_RETRY}; do \
			printf "[+] Attempting to reserve memory for hugepages...\n"; \
			 \
			echo $MEM_RES > /proc/sys/vm/nr_hugepages; \
			sleep 2; \
			 \
			if [[ "$(grep -i huge /proc/meminfo)" == *"$MEM_RES"* ]]; then
				mem_avail=1; \
				break; \
			fi; \
			echo $DEF_HUGE > /proc/sys/vm/nr_hugepages; \
			sleep 1; \
		done; \
		if [[ $mem_avail -ne 1 ]]; then \
			grep -i huge /proc/meminfo; \
			printf "[!] Failed to reserve hugepages: \n\tRequested memory is unvailable\n"; \
			echo $DEF_HUGE > /proc/sys/vm/nr_hugepages; \
			exit 1; \
		fi; \
		printf "[+] Hugepages allocated\n"; \
		echo $MAX_CPUFREQ > /sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq; \
		echo $MAX_CPUFREQ > /sys/devices/system/cpu/cpu6/cpufreq/scaling_min_freq; \
		echo $MAX_CPUFREQ > /sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq; \
		echo $MAX_CPUFREQ > /sys/devices/system/cpu/cpu7/cpufreq/scaling_min_freq; \
		echo "10de 1c20" > /sys/bus/pci/drivers/vfio-pci/new_id; \
		printf "[+] GPU passed\n"; \
		chrt -r 1 taskset -c 2,6,3,7 \
			qemu-system-x86_64 \
				-name "$VM_NAME" \
				-machine type=q35,accel=kvm \
				-global ICH9-LPC.disable_s3=1 \
				-global ICH9-LPC.disable_s4=1 \
				-cpu host,kvm=off,hv_vapic,hv_relaxed,hv_spinlocks=0x1fff,hv_time,hv_vendor_id=Screw0Nvidia \
				-smp 4,sockets=1,cores=4,threads=1 \
				-m size="$(( MEM_RES / 512 ))"G \
				-mem-path /dev/hugepages \
				-mem-prealloc \
				--device virtio-balloon \
				-rtc clock=host,base=localtime \
				-spice port=5900,image-compression=off,playback-compression=off,disable-ticketing \
				-device qxl,bus=pcie.0,addr=1c.2 \
				-vga none \
				-nographic \
				-device virtio-serial-pci \
				-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 \
				-chardev spicevmc,id=spicechannel0,name=vdagent \
				-k en-us \
				-usb \
				-device usb-tablet \
				-device usb-audio \
				-device ioh3420,bus=pcie.0,addr=1c.0,multifunction=on,port=1,chassis=1,id=root.1 \
				-device vfio-pci,host=01:00.0,bus=root.1,addr=00.0,x-pci-sub-device-id=26017,x-pci-sub-vendor-id=5464,multifunction=on,romfile=$VBIOS_ROM \
				-drive if=pflash,format=raw,readonly=on,file=$BIOS_BIN \
				-drive if=pflash,format=raw,file=$OS_VARS \
				-boot menu=on \
				-boot order=c \
				-drive id=disk0,if=ide,cache=none,format=raw,file=$OS_IMG \
				-drive id=disk1,if=virtio,cache=none,format=raw,file=$DUMMY_IMG \
				-drive file=$VIRTIO_DISK,media=cdrom \
				-drive file=$W10_ISO,media=cdrom \
				-drive file=$W7_ISO,media=cdrom \
				-net nic,model=virtio \
				-net user,smb=$SAMBA_SHARE'
	}

	return 0
} # End vm_on

vm_mem_test() {
	# Check availability of desired reverve memory.

	# check_root

	local command_name
	command_name="$(basename "$0")"

	_usage() {

		printf "[+] Usage: %s [reservable memory]

Options:
	-e        <Reservable memory in MB>
" "$command_name"

	} # End _usage

	if [[ $# -lt 2 ]]; then
		_usage
		log_w "$command_name exit with status: Invalid parameters"

		return 1
	fi

	export MEM_RES="$((($2 / 1024) * 1024 / 2))"
	export DEF_HUGE=0

	if [[ $((MEM_RES * 2048)) -ge $(($(grep -i MemTotal /proc/meminfo | sed -E 's/.*:\s*//; s/\s.*//') + 0)) || $MEM_RES -lt 0 ]]; then
		printf '[!] Unreservable memory amount\n'
		return 1
	fi

	# shellcheck disable=SC1004
	sudo --preserve-env=DEF_HUGE,MEM_RES,SUCCESS_RET bash -c ' \
	for counter in {1..4}; do \
		printf "[+] Attempting to reserve memory for hugepages...\n"; \
		 \
		echo $MEM_RES > /proc/sys/vm/nr_hugepages; \
		sleep 2; \
		 \
		if [[ "$(grep -i huge /proc/meminfo)" == *"$MEM_RES"* ]]; then
			printf "[+] Requested memory available\n"; \
			 \
			echo $DEF_HUGE > /proc/sys/vm/nr_hugepages; \
			sleep 1; \
			 \
			mem_avail=1; \
			exit $SUCCESS_RET; \
		fi; \
		echo $DEF_HUGE > /proc/sys/vm/nr_hugepages; \
		sleep 1; \
	done; \
	if [[ $mem_avail -ne 1 ]]; then \
		grep -i huge /proc/meminfo; \
		printf "[!] Failed to reserve hugepages: \n\tRequested memory is unvailable\n"; \
		echo $DEF_HUGE > /proc/sys/vm/nr_hugepages; \
		exit 1; \
	fi'
} # End vm_mem_test

#!/bin/bash

mute_mic() {
	# Mute the microphone

	# PulseAudio mixer.
	# pactl set-source-mute 1 1 && printf '[I] Mic muted\n'

	# ALSA mixer.
	amixer set Mic mute
} # End mute_mic

unmute_mic() {
	# Unmute the microphone

	# PulseAudio mixer.
	# pactl set-source-mute 1 0 && printf '[I] Mic unmuted\n'

	amixer set Mic unmute
} # End unmute_mic

scroll_lock_toggle() {
	local on_state=1
	local off_state=0

	local led_file="/sys/class/leds/input*::scrolllock/brightness"

	local current_state

	# Not quoted due to allow for globbing
	current_state=$(cat "$led_file")

	case $current_state in
	"$on_state")
		xset -led named "Scroll Lock"
		;;
	"$off_state")
		xset led named "Scroll Lock"
		;;
	esac
} # End scroll_lock_toggle

# Requires permissions to edit /sys/class/backlight/*/brightness.
# Preferably use the video group permission.
adjust_brightness() {
	local usr_input="$2"
	local adjust_percentage=10
	local current_brightness
	local max_brightness

	if [[ $usr_input =~ [+-][0-9]+ ]]; then
		adjust_percentage=$(echo "$2" | sed -E 's/[+-]//')
	fi

	if [[ -z $BACKLIGHT_CTRL_DIR ]]; then
		for dir in /sys/class/backlight/*; do
			[[ -d $dir ]] && export BACKLIGHT_CTRL_DIR="$dir"

			# Prefer acpi controlled backlight
			if [[ $dir == *"acpi_"* ]]; then
				break
			fi
		done
	fi

	current_brightness=$(($(cat "$BACKLIGHT_CTRL_DIR/actual_brightness") + 0))
	max_brightness=$(($(cat "$BACKLIGHT_CTRL_DIR/max_brightness") + 0))

	case $usr_input in
	+*)
		printf "%s: inc %s" "$BACKLIGHT_CTRL_DIR/brightness" "$adjust_percentage"

		new_brightness=$((current_brightness + ((adjust_percentage * max_brightness) / 100)))
		[[ $new_brightness -gt $max_brightness ]] && new_brightness=$max_brightness

		tee "$BACKLIGHT_CTRL_DIR/brightness" <<<$new_brightness

		# (PI) X11 specific
		# xbacklight -inc "$adjust_percentage"
		;;
	-*)
		printf "%s: dec %s" "$BACKLIGHT_CTRL_DIR/brightness" "$adjust_percentage"

		new_brightness=$((current_brightness - ((adjust_percentage * max_brightness) / 100)))
		[[ $new_brightness -lt 0 ]] && new_brightness=0

		tee "$BACKLIGHT_CTRL_DIR/brightness" <<<$new_brightness

		# (PI) X11 specific
		# xbacklight -dec "$adjust_percentage"
		;;
	*) ;;

	esac
} # End adjust_brightness

touchpad_toggle() {
	declare -i ID
	declare -i STATE

	ID="$(xinput list | grep -Eio '(touchpad|glidepoint)\s*id\=[0-9]{1,2}' | grep -Eo '[0-9]{1,2}')"

	STATE="$(xinput list-props "$ID" | grep 'Device Enabled' | awk '{print $4}')"

	if [[ $STATE -eq 1 ]]; then
		xinput disable "$ID"
		notify-send -i input-touchpad-off -a "Touchpad" "Touchpad Disabled"
	else
		xinput enable "$ID"
		notify-send -i input-touchpad-on -a "Touchpad" "Touchpad Enabled"
	fi
} # End touchpad_toggle

if [[ $# -eq 1 ]]; then
	$1
else
	$1 "$@"
fi

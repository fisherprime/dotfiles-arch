#
# /etc/bash.bashrc
#

# Sources
# ----
# `source` & `.` are the same.
source_files=(
	"/usr/share/doc/pkgfile/command-not-found.bash "
	"/usr/share/bash-completion/bash_completion "
	"/usr/share/doc/pkgfile/command-not-found.bash "
)

for source_file in "${source_files[@]}"; do
	# shellcheck source=/dev/null
	[[ -r "$source_file" ]] && . "$source_file"
done
# ----

# Exports
# ----
# Shell history
export HISTCONTROL=ignoreboth:erasedups
export HISTFILE="$HOME/.bash_history"
export HISTFILESIZE=10000
export HISTSIZE=11000
# ----

# Shell options
# ----
# REF: `man bash`
set -o vi

# shopt -s progcomp
shopt -s autocd
shopt -s cdable_vars
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s globstar
shopt -s histappend
shopt -s hostcomplete
shopt -s nullglob
# ----

# Completions
# ----
for file in /etc/bash_completion.d/*; do
	# shellcheck source=/dev/null
	[[ -r $file ]] && . "$file"
done

# Add skim's fuzzy autocomplete
# Load last to avoid overriding
[[ -d /usr/share/skim/ && $- == *i* ]] &&
	. "/usr/share/skim/key-bindings.bash" &&
	. "/usr/share/skim/completion.bash"
# ----

# vim: ft=sh

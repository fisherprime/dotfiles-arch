"
" ~/.vim/configs/lang.vim
"

" Go

" Run GoMetaLinter on save.
"let g:go_metalinter_autosave = 1

" Specify the command to run on `:GoMetaLinter`.
let g:go_metalinter_command = 'golangci-lint'
let g:go_metalinter_enabled = []
let g:go_metalinter_deadline = '30s'

" Rust
let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0

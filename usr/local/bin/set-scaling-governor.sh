#!/bin/bash

# Alter the CPU governor to a user specified one.
#
# Arguments: <cpu-governor>
#
# This operation expects a single argument, else it fails
[[ $# -eq 1 ]] &&
	tee /sys/devices/system/cpu/cpufreq/policy*/scaling_governor <<<"$1"

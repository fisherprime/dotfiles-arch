# INET (IPv4 & IPv6) filter rules.

# ----
table inet filter {
	set tcp_accepted {
		type inet_service; flags interval;

		# `http` port => 80.
		# `https` port => 443.

		define custom_1 = 62456
		define custom_2 = 35000

		# Syncthing communication port => 22000.
		define syncthing_comm = 22000

		elements = {
			$custom_1, $custom_2, ssh, $syncthing_comm
		}
	}

	set udp_accepted {
		type inet_service; flags interval;

		# (BOOTP)/`bootps` Bootstrap protocol, for IP assignment.
		# `bootps` port => 67 (server) | => 68 (client).
		# `mdns` port => 5353.
		# `openvpn` port => 1194.

		# Syncthing administration port => 21027.
		define syncthing_admin = 21027

		elements = {
			bootps, mdns, $syncthing_admin
		}
	}

	# set docker_accepted {
		# # Django
		# # SSH
		# # Elasticsearch
		# type inet_service; flags interval;
#
		# elements = {
			# 8021, 1122, 9031
			# }
		# }

	chain base_checks {
		# Allow established & related connections.

		# ct taps into the connection tracking entry of a packet.
		ct state {established, related} counter accept comment "Accept valid ct (base_checks)"

		# Drop invalid connections.
		ct state invalid counter drop comment "Drop invalid ct (base_checks)"
	}

	chain drop_nets_unwanted {
		# Drop packets from specified dangerous IPs.
		ip saddr $nets_v4_blocked counter drop comment "Drop unwanted IPv4 inbound"
		ip daddr $nets_v4_blocked counter drop comment "Drop unwanted IPv4 outbound"

		# ip saddr $nets_v6_blocked counter drop comment "Drop unwanted IPv6 input"
		# ip daddr $nets_v6_blocked counter drop comment "Drop unwanted IPv6 outbound"
	}

	chain accept_virtualized_input {
		# Accept libvirt packets.
		ip daddr $nets_libvirt oifname $nics_libvirt counter accept comment "Accept Libvirt inbound traffic (input-INET)"
		ip saddr $nets_libvirt iifname $nics_libvirt counter accept comment "Accept Libvirt outbound traffic (input-INET)"

		# Accept Docker packets.
		ip daddr $nets_docker oifname $nics_docker counter accept comment "Accept Docker inbound traffic (input-INET)"
		ip saddr $nets_docker iifname $nics_docker counter accept comment "Accept Docker outbound traffic (input-INET)"
	}

	chain accept_trusted_input {
		# Accept packets from trusted networks on trusted interfaces & common ports.

		# Allow pings (ICMP & ICMPv6).
		ip protocol icmp icmp type {echo-request, echo-reply, time-exceeded, parameter-problem, destination-unreachable} counter accept comment "Accept ICMP (input-INET)"
		# ip6 nexthdr icmpv6 icmpv6 type {echo-request, echo-reply, time-exceeded, parameter-problem, destination-unreachable, packet-too-big, nd-router-advert, nd-router-solicit, nd-neighbor-solicit, nd-neighbor-advert, mld-listener-query} counter accept comment "Accept ICMPv6 (input-INET)"

		# Accept loopback packets.
		iifname lo counter accept comment "Accept loopback (input-INET)"

		# Accept packets for common & trusted services.
		tcp dport @tcp_accepted counter accept comment "Accept TCP predefined (input-INET)"
		udp dport @udp_accepted counter accept comment "Accept UDP predefined (input-INET)"

		ip saddr $nets_trusted iifname $nics_trusted counter accept comment "Accept trusted networks' inbound traffic (input-INET)"

		jump accept_virtualized_input
	}

	chain reject_respond {
		# Reject with a response; an alternative to dropping packets.
		# NOTE: The pundits prefer this to dropping packets as it allows the
		# connection to terminate without further retry attempts.
		counter reject with icmpx type port-unreachable comment "Reject unknown inbound traffic (input-INET)"
	}

	chain generic_trace {
		# Trace packets processed by this rule with a defined message prefix.
		counter log prefix "INET trace: " meta nftrace set 1 comment "(INET)"
	}

	chain input {
		# Define chain of type filter, working on incoming packets, with a default policy of
		# dropping packets.
		type filter hook input priority filter; policy drop;

		# An anonymous counter for all packets processed by the current chain.
		counter comment "(input-INET)"

		# Jump to chain generic_trace & return to this chain on completion.
		# jump generic_trace

		jump base_checks
		jump drop_nets_unwanted

		# pkttype host limit rate 5/second counter reject with icmpx type admin-prohibited
		jump accept_trusted_input

		jump reject_respond
	}

	chain accept_trusted_forward {
		# Allow forwarding packets from trusted networks.
		ip saddr $nets_private iifname $nics_forward_source counter accept comment "Allow trusted forward traffic (forward-INET)"
	}

	chain forward {
		# Define chain of type filter, working on packets being forwarded, with a default policy of
		# dropping packets.
		type filter hook forward priority filter; policy drop;

		counter comment "(forward-INET)"
		# jump generic_trace

		jump base_checks
		jump drop_nets_unwanted

		jump accept_trusted_forward

		counter comment "Drop unknown forward traffic (forward-INET)"
	}

	chain output {
		# Accept all outgoing packets to networks not blacklisted.
		type filter hook output priority filter; policy accept;

		counter comment "(output-INET)"
		# jump generic_trace

		jump drop_nets_unwanted

		# Allow output to "safe" networks: no malware, tracking, ...
		counter comment "Allow safe outbound traffic (output-INET)"
	}
}
# ----
